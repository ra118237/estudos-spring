package br.com.william.demorestapi.endpoint;

import javax.websocket.server.PathParam;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.william.demorestapi.entity.Mensagem;

@RestController
@RequestMapping("/saudacoes")
public class HelloWorld {

	@GetMapping
	private ResponseEntity<Mensagem> dizOla(){
		return ResponseEntity.ok(new Mensagem(1L, "Ola mundo !!!"));
		
	}
	
	@GetMapping("/{nome}")
	private ResponseEntity<Mensagem> dizOla(@PathVariable String nome){
		return ResponseEntity.ok(new Mensagem(1L, "Ola "+nome));
		
	}
}
