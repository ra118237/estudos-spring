package br.com.william.springstudy;

public class LetterService {
  public void sendLetter(String receiver, String message){
    System.out.println("Enviando carta  para "+receiver+" com a msg:");
    System.out.println("  - "+message);
  }
}
