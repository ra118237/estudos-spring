package br.com.william.springstudy;

import br.com.william.springstudy.config.ConfigurationApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main( String[] args ){
//        LetterService letterService = new LetterService();

//        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("spring.xml"));//deprecated
//        LetterService letterService = beanFactory.getBean("letterService", LetterService.class);

//        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring.xml");
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ConfigurationApplicationContext.class);

        LetterService letterService = applicationContext.getBean("letterService", LetterService.class);

        letterService.sendLetter("John William", "Ola");
    }
}
