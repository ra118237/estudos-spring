package br.com.william.springstudy.config;

import br.com.william.springstudy.LetterService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigurationApplicationContext {

    @Bean
    public LetterService letterService(){
      return new LetterService();
    }
}
